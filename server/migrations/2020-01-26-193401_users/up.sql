CREATE TYPE scopesType AS ENUM ('profile');

CREATE TABLE users (
  email VARCHAR(100) NOT NULL PRIMARY KEY,
  hash VARCHAR(122) NOT NULL, --argon hash
  created_at TIMESTAMP NOT NULL,
  scopes scopesType[] NOT NULL
);

-- Use to add new scopes
--ALTER TYPE scopesType ADD VALUE 'new_value'; -- appends to list
--ALTER TYPE scopesType ADD VALUE 'new_value' BEFORE 'old_value';
--ALTER TYPE scopesType ADD VALUE 'new_value' AFTER 'old_value';
