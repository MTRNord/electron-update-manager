use std::collections::HashSet;
use std::io::Write;
use std::iter::FromIterator;
use std::str::FromStr;

use actix_web::http::Method;
use actix_web::web;
use actix_web::HttpRequest;
use chrono::Utc;
use diesel::pg::Pg;
use diesel::serialize::IsNull;
use diesel::{
    deserialize::{self, FromSql},
    serialize::{self, Output, ToSql},
};
use diesel::{r2d2::ConnectionManager, PgConnection};

use crate::errors::ServiceError;
use crate::jwt::get_claims;

use super::schema::*;

// type alias to use in multiple places
pub type Pool = r2d2::Pool<ConnectionManager<PgConnection>>;

#[derive(Debug, Serialize, Deserialize, Queryable, Insertable)]
#[table_name = "invitations"]
pub struct Invitation {
    pub id: uuid::Uuid,
    pub email: String,
    pub expires_at: chrono::NaiveDateTime,
}

// any type that implements Into<String> can be used to create Invitation
impl<T> From<T> for Invitation
where
    T: Into<String>,
{
    fn from(email: T) -> Self {
        Invitation {
            id: uuid::Uuid::new_v4(),
            email: email.into(),
            expires_at: chrono::Local::now().naive_local() + chrono::Duration::hours(24),
        }
    }
}

#[derive(SqlType)]
#[postgres(type_name = "scopes_type")]
pub struct Scopestype;

#[derive(
    AsStaticStr, Debug, Hash, Eq, PartialEq, Clone, Serialize, Deserialize, FromSqlRow, AsExpression,
)]
#[sql_type = "Scopestype"]
pub enum Scopes {
    Profile,
    Apps,
}

impl FromStr for Scopes {
    type Err = String;

    fn from_str(s: &str) -> Result<Scopes, String> {
        match s {
            "profile" => Ok(Scopes::Profile),
            "apps" => Ok(Scopes::Apps),
            e => Err(format!("Unable to parse: {}", e)),
        }
    }
}

impl ToSql<Scopestype, Pg> for Scopes {
    fn to_sql<W: Write>(&self, out: &mut Output<W, Pg>) -> serialize::Result {
        match *self {
            Scopes::Profile => out.write_all(b"profile")?,
            Scopes::Apps => out.write_all(b"apps")?,
        }
        Ok(IsNull::No)
    }
}

impl FromSql<Scopestype, Pg> for Scopes {
    fn from_sql(bytes: Option<&[u8]>) -> deserialize::Result<Self> {
        match not_none!(bytes) {
            b"profile" => Ok(Scopes::Profile),
            b"apps" => Ok(Scopes::Apps),
            _ => Err("Unrecognized enum variant".into()),
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Queryable, Insertable, PartialEq)]
#[table_name = "users"]
pub struct User {
    pub email: String,
    pub hash: String,
    pub created_at: chrono::NaiveDateTime,
    pub scopes: Vec<Scopes>,
}

impl User {
    pub fn from_details<S: Into<String>, T: Into<String>>(
        email: S,
        pwd: T,
        scopes: Vec<Scopes>,
    ) -> Self {
        User {
            email: email.into(),
            hash: pwd.into(),
            created_at: chrono::Local::now().naive_local(),
            scopes,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SlimUser {
    pub email: String,
    pub scopes: Vec<Scopes>,
}

impl From<User> for SlimUser {
    fn from(user: User) -> Self {
        SlimUser {
            email: user.email,
            scopes: user.scopes,
        }
    }
}

pub struct UserGuard {
    pub required_scopes: Vec<Scopes>,
    pub pool: web::Data<Pool>,
}

#[inline]
fn hashset(data: &Vec<Scopes>) -> HashSet<Scopes> {
    HashSet::from_iter(data.iter().cloned())
}

impl UserGuard {
    pub fn check(&self, request: &HttpRequest) -> Result<bool, ServiceError> {
        // Ignore POST as it is the login method
        if request.method() == Method::POST && request.uri().to_string().contains("/api/auth") {
            return Ok(true);
        }

        // Block any other request if it does not have the required scopes
        if request.headers().contains_key("authorization") {
            let claims = match get_claims(request) {
                Ok(c) => c,
                Err(e) => return Err(e),
            };

            if claims.exp != Utc::now().timestamp() {
                // Check if user exists
                use crate::schema::users::dsl::{email, users};
                use diesel::prelude::*;
                let conn: &PgConnection = &self.pool.get().unwrap();
                let user = users
                    .filter(email.eq(&claims.sub))
                    .first::<User>(conn)
                    .expect("Unable to get user from database");

                if user.email == claims.sub {
                    let scopes_token_hashset = hashset(&claims.scopes);

                    let required_scopes_hashset = hashset(&self.required_scopes);

                    let intersection = scopes_token_hashset
                        .intersection(&required_scopes_hashset)
                        .map(|x| x.to_owned())
                        .collect::<Vec<Scopes>>();

                    return if intersection == self.required_scopes {
                        Ok(true)
                    } else {
                        Ok(false)
                    };
                }
            }
        }
        Ok(false)
    }
}
