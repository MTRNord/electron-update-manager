use chrono::Utc;
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, Validation};

use crate::errors::ServiceError;
use crate::models::Scopes;
use crate::utils;
use actix_web::HttpRequest;

/// Our claims struct, it needs to derive `Serialize` and/or `Deserialize`
#[derive(Debug, Serialize, Deserialize)]
pub struct Claims {
    pub sub: String,
    pub scopes: Vec<Scopes>,
    pub exp: i64,
}

pub async fn get_token(email: String, scopes: Vec<Scopes>) -> Option<String> {
    let claims = Claims {
        sub: email.to_owned(),
        scopes,
        exp: Utc::now().timestamp() + 3600 * 24,
    };

    let token = match encode(
        &Header::default(),
        &claims,
        &EncodingKey::from_secret(utils::JWT_SHARED_SECRET.as_ref()),
    ) {
        Ok(t) => t,
        Err(_) => return None,
    };

    Some(token)
}

pub fn decode_token(token: String) -> Result<Claims, ServiceError> {
    let token = match decode::<Claims>(
        &token,
        &DecodingKey::from_secret(utils::JWT_SHARED_SECRET.as_ref()),
        &Validation::default(),
    ) {
        Ok(t) => Ok(t.claims),
        Err(_) => Err(ServiceError::InvalidToken),
    };

    token
}

pub fn get_claims(request: &HttpRequest) -> Result<Claims, ServiceError> {
    let token_raw = request
        .headers()
        .get("Authorization")
        .unwrap()
        .to_str()
        .unwrap()
        .replace("Bearer ", "");

    let claims = match decode_token(token_raw) {
        Ok(c) => Ok(c),
        Err(e) => Err(e),
    };
    claims
}
