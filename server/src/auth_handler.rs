use actix_web::{error::BlockingError, web, Either, HttpRequest, HttpResponse, Responder};
use diesel::prelude::*;
use diesel::PgConnection;

use crate::errors::ServiceError;
use crate::jwt::{get_claims, get_token};
use crate::models::{Pool, Scopes, SlimUser, User, UserGuard};
use crate::utils::verify;

#[derive(Debug, Deserialize)]
pub struct AuthData {
    pub email: String,
    pub password: String,
}

pub async fn logout() -> HttpResponse {
    // TODO implement logout/token invalidation
    HttpResponse::NotImplemented().finish()
}

pub async fn login(auth_data: web::Json<AuthData>, pool: web::Data<Pool>) -> impl Responder {
    let res: Result<SlimUser, BlockingError<ServiceError>> =
        web::block(move || query(auth_data.into_inner(), pool)).await;
    return match res {
        Ok(user) => Ok(HttpResponse::Ok().body(get_token(user.email, user.scopes).await.unwrap())),
        Err(err) => match err {
            BlockingError::Error(service_error) => Err(service_error),
            BlockingError::Canceled => Err(ServiceError::InternalServerError),
        },
    };
}

pub async fn get_me(req: HttpRequest, pool: web::Data<Pool>) -> Either<HttpResponse, ServiceError> {
    let guard = UserGuard {
        required_scopes: vec![Scopes::Profile],
        pool,
    }
    .check(&req);

    if guard.is_ok() {
        if !guard.unwrap() {
            return Either::B(ServiceError::Unauthorized);
        }
    } else {
        return Either::B(guard.err().unwrap());
    }

    let claims = match get_claims(&req) {
        Ok(c) => c,
        Err(e) => return Either::B(e),
    };
    Either::A(HttpResponse::Ok().json(claims))
}

/// Diesel query
fn query(auth_data: AuthData, pool: web::Data<Pool>) -> Result<SlimUser, ServiceError> {
    use crate::schema::users::dsl::{email, users};
    let conn: &PgConnection = &pool.get().unwrap();
    let mut items = users
        .filter(email.eq(&auth_data.email))
        .load::<User>(conn)?;

    if let Some(user) = items.pop() {
        if let Ok(matching) = verify(&user.hash, &auth_data.password) {
            if matching {
                return Ok(user.into());
            }
        }
    }
    Err(ServiceError::Unauthorized)
}
