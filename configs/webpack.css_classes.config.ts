import {CleanWebpackPlugin} from 'clean-webpack-plugin';
import * as webpack from "webpack";
import * as path from "path";
// @ts-ignore
import * as  WebpackBarPlugin from "webpackbar";

// Webpack generates `css_classes.rs` with this config.
// This config is used in command `yarn generate:css_classes`.
// See `webpack.config.ts` for more info about individual settings.

export default (_env: any, argv: { mode: any; }) => {
    const config: webpack.Configuration = {
        entry: path.resolve(__dirname, "../entries/index.css_classes.ts"),
        output: {
            path: path.resolve(__dirname, "../dist"),
            filename: "css_classes.js"
        },
        plugins: [new WebpackBarPlugin(), new CleanWebpackPlugin()],
        module: {
            rules: [
                {
                    test: /\.(jpg|jpeg|png|woff|woff2|eot|ttf|svg)$/,
                    use: [
                        {
                            loader: "file-loader",
                            options: {
                                emitFile: false,
                                name: "[path][name].[ext]"
                            }
                        }
                    ]
                },
                {
                    test: /\.ts$/,
                    loader: "ts-loader?configFile=configs/tsconfig.css_classes.json"
                },
                {
                    test: /\.css$/,
                    use: [
                        "style-loader",
                        "css-loader",
                        {
                            loader: "postcss-loader",
                            options: {
                                config: {
                                    ctx: {mode: argv.mode},
                                    path: __dirname
                                }
                            }
                        }
                    ]
                }
            ]
        }
    };
    return config;
}
