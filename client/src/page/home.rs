use seed::{prelude::*, *};

use crate::Msg;

#[allow(clippy::too_many_lines)]
pub fn view() -> impl View<Msg> {
    h1!["Let there be light"]
}
