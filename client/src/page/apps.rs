use crate::generated::css_classes::C;
use seed::{prelude::*, *};

use crate::Msg;

#[allow(clippy::too_many_lines)]
pub fn view() -> impl View<Msg> {
    div![
        h1!["Apps"],
        button![
            class![C.btn, C.btn_primary,],
            attrs! {
                At::Type => "button",
                At::Custom("data-toggle".to_string()) => "modal",
                At::Custom("data-target".to_string()) => "#addAppModal"
            },
            "Add App"
        ],
        div![
            class![C.modal, C.fade],
            id!("addAppModal"),
            attrs! {
                At::Custom("tabindex".to_string()) => "-1",
                At::Custom("role".to_string()) => "dialog",
                At::Custom("aria-labelledby".to_string()) => "addAppModalLabel",
                At::Custom("aria-hidden".to_string()) => "true"
            },
            div![
                class![C.modal_dialog],
                attrs! {
                    At::Custom("role".to_string()) => "document"
                },
                div![
                    class![C.modal_content],
                    div![
                        class![C.modal_header],
                        h5![
                            class![C.modal_title],
                            id!("addAppModalLabel"),
                            "Modal title"
                        ],
                        button![
                            class![C.close,],
                            attrs! {
                                At::Type => "button",
                                At::Custom("data-dismiss".to_string()) => "modal",
                                At::Custom("aria-label".to_string()) => "Close"
                            },
                            span![
                                attrs! {
                                    At::Custom("aria-hidden".to_string()) => "true"
                                },
                                "×"
                            ]
                        ]
                    ],
                    div![class![C.modal_body], "Wow"],
                    div![
                        class![C.modal_footer],
                        button![
                            class![C.btn, C.btn_secondary,],
                            attrs! {
                                At::Type => "button",
                                At::Custom("data-dismiss".to_string()) => "modal"
                            },
                            "Close"
                        ],
                        button![
                            class![C.btn, C.btn_primary,],
                            attrs! {
                                At::Type => "button"
                            },
                            "Save changes"
                        ]
                    ]
                ]
            ]
        ]
    ]
}
