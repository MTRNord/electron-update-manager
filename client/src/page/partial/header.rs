use crate::{generated::css_classes::C, Model, Msg, Page};
use seed::{prelude::*, *};

#[allow(clippy::too_many_lines)]
pub fn view(model: &Model) -> impl View<Msg> {
    header![nav![
        class![C.navbar, C.navbar_expand_lg, C.navbar_light, C.bg_light],
        a![
            class![C.navbar_brand,],
            attrs! {
                At::Href => Page::Home.to_href()
            },
            "Electron Update Manager"
        ],
        button![
            class![C.navbar_toggler,],
            attrs! {
                At::Type => "button",
                At::Custom("data-toggle".to_string()) => "collapse",
                At::Custom("data-target".to_string()) => "#navbarNav",
                At::Custom("aria-controls".to_string()) => "navbarNav",
                At::Custom("aria-expanded".to_string()) => "false",
                At::Custom("aria-label".to_string()) => "Toggle navigation",
            },
            span![class![C.navbar_toggler_icon,],]
        ],
        div![
            class![C.collapse, C.navbar_collapse,],
            id!("navbarSupportedContent"),
            ul![
                class![C.navbar_nav, C.mr_auto,],
                li![
                    class![
                        C.nav_item,
                        C.active => model.page == Page::Home,
                    ],
                    a![
                        class![C.nav_link],
                        attrs! {
                            At::Href => Page::Home.to_href()
                        },
                        "Home",
                        span![class![C.sr_only,], "(current)"]
                    ]
                ]
            ]
        ]
    ],]
}
