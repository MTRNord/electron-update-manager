import "../css/styles.css";

(async () => {
  // Note: files in `client/pkg/` will be created on the first build.
  await import("../client/pkg/index");
})();
